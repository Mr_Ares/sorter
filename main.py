from watchdog.observers import Observer
import os
from watchdog.events import FileSystemEventHandler
from time import sleep

from Config import *
from funs import *


def sort():
    for object_name in os.listdir(folder_track):
        if is_folder(object_name):
            continue

        file_name = object_name
        file_extension = file_name.split(".")[-1].lower()
        file_path = folder_track + '/' + file_name
        # не даёт переносить недогруженые браузером файлы
        if object_name == 'desktop.ini' or file_extension == 'tmp'\
                or file_extension == 'crdownload':
            continue

        for file_type, file_type_extensions in extensions.items():
            if file_extension in file_type_extensions:
                try:
                    new_file_path = folders[file_type] + '/' + new_name(file_name, file_type)
                    os.rename(file_path, new_file_path)
                    break
                except FileNotFoundError:
                    os.mkdir(folders[file_type], 0o777)
                    new_file_path = folders[file_type] + '/' + new_name(file_name, file_type)
                    os.rename(file_path, new_file_path)
                    break
                except FileExistsError:
                    os.remove(file_path)
        else:
            try:
                new_file_path = folders['other'] + '/' + new_name(file_name, file_type)
                os.rename(file_path, new_file_path)
            except FileNotFoundError:
                try:
                    os.mkdir(folders['other'], 0o777)
                    new_file_path = folders['other'] + '/' + new_name(file_name, file_type)
                    os.rename(file_path, new_file_path)
                    break
                except FileExistsError:
                    pass
            except FileExistsError:
                os.remove(file_path)


class Handler(FileSystemEventHandler):
    def on_modified(self, event):
        sleep(sleep_time)
        sort()


def main():
    handler = Handler()
    observer = Observer()
    observer.schedule(handler, folder_track, True)
    observer.start()
    observer.join()


if __name__ == '__main__':
    if start_sort:
        sort()
    main()
