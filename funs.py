import time
import Config


def new_name(file_name, file_type):
    if file_type in Config.rename_files_types:
        return time.strftime('%Y_%m_%d  %H-%M-%S.' + file_name.split(".")[-1].lower())
    return file_name


def is_folder(full_name):
    name = full_name.split(".")
    if len(name) == 1:
        return True
    return False
