# Сортировка при запуске
start_sort = True

# Переименование файлов в дату сортировки
rename_files_types = 'photos'

# Папка которая отслеживается
folder_track = 'C:/Users/dimae/Downloads'

# Базовая папка куда сортируются файлы (используется только в словаре ниже)
# Используется только в словаре ниже для создания абсолютных путей
# Можно прописать свои пути в ручную, но пути должны быть абсолютные!!!
folder_output = 'C:/Users/dimae/Downloads'

folders = {
    'photos': folder_output + '/Фото',
    'videos': folder_output + '/Видео',
    'music': folder_output + '/Музыка',
    'apps': folder_output + '/Приложения',
    'archives': folder_output + '/Архивы',
    'texts': folder_output + '/Тексты',
    'presentations': folder_output + '/Презентации',
    'tables': folder_output + '/Таблицы',
    'python': folder_output + '/Python',
    'other': folder_output + '/Остальное'
}

# Сюда можно добовлять свои типы файлов и их расширения
# Не забудьте добавить свои типы в словарь с путями выше!!!
extensions = {
    'photos': ['jpg', 'bmp', 'gif', 'tiff', 'png'],
    'videos': ['avi', 'wmf', '3gp', 'mp4', 'mpg2'],
    'music': ['mp3', 'wma'],
    'apps': ['exe', 'cmd', 'bat', 'msi'],
    'archives': ['rar', 'zip', 'tg', '7z'],
    'texts': ['txt', 'doc', 'rtf', 'docx', 'pdf'],
    'presentations': ['ppt', 'pptx'],
    'tables': ['xls', 'xlsm', 'xlsx', 'xml', 'xlsb'],
    'python': ['py', 'pyx']
}

# время до сортировки после добавления файла в директорию
sleep_time = 120
